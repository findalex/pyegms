#!/usr/bin/python

import sys
sys.dont_write_bytecode = True # no .pyc files

#
# pygamess functions
#

def pyg_print_list_emsl_basis_names(atoms):
    
    """ 
    IN:  list of strings ['H', 'C', 'Pt', etc.]
    OUT: prints out only those EMSL basis-sets 
         that are commonly available
    """
    
    from ebsel import EMSL_local
    basis_name_list = []
    emsl_gms = EMSL_local(fmt='gamess-us')
    for x in emsl_gms.get_available_basis_sets(atoms):
        print x[0].ljust(35)+x[1]

def pyg_get_list_emsl_basis_names(atoms):
    
    """
    IN:  list of strings ['H', 'C', 'Pt', etc.] 
    OUT: returns a list of strings; names of EMSL basis-sets
         that are commonly available.
    """
    
    from ebsel import EMSL_local
    basis_name_list = []
    emsl_gms = EMSL_local(fmt='gamess-us')
    for x in emsl_gms.get_available_basis_sets(atoms):
        basis_name_list.append(x[0].ljust(35)+x[1])
    return basis_name_list

def pyg_get_emsl_basis_for_element(atom,basis_name):

    """ 
    IN:  element symbol and basis-set name (strings)
    OUT: returns the requested basis-set as a formatted
         string. 

    NOTES: The local EMSL 'API' will mess up for symbols longer than one character 
           (i.e. Pt, Mn) unless the symbol string is in a list.  So 'atom' is
           passed to the API function inside of a list explicitly in the call
           argument (see below).
    """
    
    from ebsel import EMSL_local
    emsl_gms = EMSL_local(fmt='gamess-us')
    basis_set = emsl_gms.get_basis(basis_name,[atom])
    return basis_set

#
# pyg_molecule class and member functions
#

class pyg_molecule:
    
    """
    IN: path to file containing molecule coordinates
    """
    
    def __init__(self,path):

        """ 
        IN:path to coordiantes 
        """

        # import the regular expression module
        import re
        
        # initialize some class variables
        self.xyz = []; self.unique_xyz = []
        self.atoms = []; self.unique_atoms = []
        self.charges = []; self.unique_charges = []
        self.size = 0; unique_size = []
        self.symmetry = 'n/a'
        self.comment = 'n/a'
        self.basis_set = []

        # these tell the program when it is parsing symmetry unique
        # coordinates or all coordiantes in a file.
        symm_coord = False; all_coord = False
        
        # compile re pattern for GAMESS $DATA group.
        # \s is re for whitespace and the modifier * means look for
        # 0 or more \s's in a row.  \w* is alpha-numeric, \d is numeric,
        # and \-? means look for an optional (?)  negative sign (\-).
        data_temp = re.compile(
            '\s*(\w*)\s*(\d*\.\d*)'+\
            '\s*(\-?\d*\.\d*)'+\
            '\s*(\-?\d*\.\d*)'+\
            '\s*(\-?\d*\.\d*)'+\
            '\s')

        # check if path leads to a GAMESS .log file.
        if path[-3:]=='log':

            # read the lines of the .inp file into the list *lines*.
            lines = open(path).readlines()            

            # iterate over *lines* for every line; n is an index for each line and is
            # incremented from 0->len(lines).
            for n,line in enumerate(lines):

                if 'TOTAL NUMBER OF ATOMS' in line: self.size = int(line.split()[-1])
                if 'THE POINT GROUP IS' in line:
                    self.symmetry = (line.split()[4]+line.split()[6]).replace(',','')
                    if 'C1' in self.symmetry:
                        self.symmetry='C1'
                # only check for re match in appropriate region of the log file.
                if 'COORDINATES OF SYMMETRY UNIQUE' in line:
                    all_coord = False; symm_coord = True
                if 'COORDINATES OF ALL ATOMS ARE' in line:
                    all_coord = True; symm_coord = False 
                # attempt a match between data_temp pattern and the line.
                data = re.match(data_temp,line)                

                # if the current line matches the re (if data = 1) and
                # symm_coord is True, then we... 
                if data and symm_coord:
                    # ...append the atom name to the list of sym unique atoms,
                    self.unique_atoms.append(data.group(1))
                    self.unique_charges.append(data.group(2))
                    # and append the 1-D list of xyz coordinates of sym unique atoms
                    # to the list of lists self.unique_xyz.
                    self.unique_xyz.append([data.group(i) for i in range(3,6)])

                # if the current line matches the re (if data = 1) and
                # all_coord is True, then we... 
                elif data and all_coord:
                    # ...append the atom name to the list of atoms,
                    self.atoms.append(data.group(1))
                    self.charges.append(data.group(2))                    
                    # and append the 1-D list of xyz coordinates
                    # to the list of lists self.xyz.
                    self.xyz.append([data.group(i) for i in range(3,6)])

                # once all atoms are accounted for...
                elif len(self.atoms)==self.size:
                    all_coord=False; symm_coord=False            

            # use the length of the self.atoms list to set the
            # self.unique_size class variable.
            self.unique_size = len(self.unique_atoms)
            
            
        # checking for GAMESS .inp file
        if path[-3:]=='inp':

            # read the lines of the .inp file into the
            # list *lines*.
            lines = open(path).readlines()

            # iterate over *lines* for every line;
            # n is an index for each line and is
            # incremented from 0->len(lines).
            for n,line in enumerate(lines):

                # check for the $DATA group
                if '$DATA' in line and path[-3:]=='inp':
                    # parse .inp comment from $DATA if available.
                    self.comment = lines[n+1]
                    # parse symmetry given in $DATA.
                    self.symmetry = ' '.join(lines[n+2].split()[0:2])

                # use reqular expresions (re module) to parse $DATA
                data = re.match(data_temp,line)

                if data:
                    # if the current line matches the re (if data = 1),
                    # append the atom name to the list of atoms,
                    if 'c1' in self.symmetry or 'C1' in self.symmetry:
                        self.atoms.append(data.group(1))
                        self.charges.append(data.group(2))                        
                        # and append the 1-D list of xyz coordinates
                        # to the list of lists self.xyz.
                        self.xyz.append([data.group(i) for i in range(3,6)])
                        # use the length of the self.atoms list to set the
                    else:
                        self.unique_atoms.append(data.group(1))
                        self.unique_charges.append(data.group(2))                        
                        # and append the 1-D list of xyz coordinates
                        # to the list of lists self.xyz.
                        self.unique_xyz.append([data.group(i) for i in range(3,6)])
                        # use the length of the self.atoms list to set the

            # self.size class variable.
            self.size = len(self.atoms)
            self.unique_size = len(self.unique_atoms)            

        # checking for .xyz formatted file
        if path[-3:]=='xyz':

            # read the lines into the list lines
            lines = open(path).readlines()

            # get the number of atoms from the .xyz file
            self.size = int(lines[0].split()[0])

            # get the comment from the .xyz file
            self.comment = lines[1]

            # read the rest of the lines into self.atoms
            # and self.xyz. GAMESS .inp files are more
            # complicated than .xyz files; we can get away
            # with using split over re with .xyz.
            for line in lines[2:]:
                self.atoms.append(line.split()[0])
                self.xyz.append([x for x in line.split()[1:]])

        # if self.xyz = 0, then we only have the symmetry unique coordinates
        if len(self.xyz)==0:
            print('only the symmetry unique coordinates are known')

        for num,x in enumerate(self.atoms):
            if len(x)>1:
                tmp = x[-1].lower()
                self.atoms[num]=x[:-1]+tmp            

        for num,x in enumerate(self.unique_atoms):
            if len(x)>1:
                tmp = x[-1].lower()
                self.unique_atoms[num]=x[:-1]+tmp            
                
        if self.symmetry=='C1':
            atoms_set = list(set(self.atoms))
        else:
            atoms_set = list(set(self.unique_atoms))
        self.atom_set = atoms_set
            
    def ___len__(self):
        return self.size

    def __iter__(self):
        for elem in self.xyz:
            yield elem

    def mol_write_xyz(self,name,comment):

        """
        IN: path to xyz file to be written; a comment to include in the comment line.
        OUT: generates a file
        """
        # check if you have the full atom/coordinate list, and not just symmetry uniques.
        if self.size==0:
            print('I do not want to write a .xyz containing just symmetry unique atoms.')
            print('you have killed pygms... :(')
            return

        # open the new xyz file and write the size, comment and coordinates.
        xyz = open(name,'w')
        xyz.write(str(self.size)+'\n')
        xyz.write(comment+'\n')
        for i in range(self.size):
            xyz.write(self.atoms[i]+' '*5+'  '.join(self.xyz[i])+'\n')

    def mol_write_data_group(self,name,comment,nosym=False):

        """
        IN:  path to write file; comment; flag to force C1 symmetry.
        OUT: writes $data group to file.
        """
        
        inp = open(name,'a')
        inp.write(' $DATA\n')
        inp.write(comment+'\n')
        if nosym or self.symmetry=='C1':
            if self.size==0:
                print('You are trying to write C1 coordiantes that you do not have') ; exit
            inp.write('C1\n')
            for i in range(self.size):
                inp.write(self.atoms[i]+' '*2+
                          self.charges[i]+' '*5+
                          '  '.join(self.xyz[i])+'\n')
        else:
            inp.write(self.symmetry+'\n'*2)
            for i in range(self.unique_size):
                inp.write(self.unique_atoms[i]+' '*2+
                          self.unique_charges[i]+' '*5+
                          '  '.join(self.unique_xyz[i])+'\n')
                
        inp.write(' $END\n')

    def mol_write_basis_group(self,name):

        """
        IN:  path to write file.
        OUT: writes $basis related groups to file.
        """
        
        if len(self.atoms)==0:
            print 'Full atom list missing!'
            print 'Maybe you only have symmetry unique atoms?'
            print 'Symmetry engine not yet implemented'
            return
        else:
            inp = open(name,'a')
            inp.write(' $BASIS  basnam(1)=\n')
            for num,atom in enumerate(self.atoms):
                inp.write(atom)
                if num<(len(self.atoms)-1):
                    inp.write(',\n')
                else:
                    inp.write('\n $END\n')
            for num,atom in enumerate(self.atom_set):
                inp.write(' $'+atom+' ! basis functions for ')
                for basis in self.basis_set:
                    if atom==basis[0]:
                        inp.write(basis[1]+' '+basis[2]+'\n\n $END\n')
            inp.close()
        
    def mol_write_ecp_group(self,name):

        """
        IN:  path to write file.
        OUT: writes $ecp related groups to file.
        """

        if len(self.atoms)==0:
            print 'Full atom list missing!'
            print 'Maybe you only have symmetry unique atoms?'
            print 'Symmetry engine not yet implemented'
        else:
            inp = open(name,'a')
            inp.write(' $CONTRL PP=READ $END\n')
            inp.write(' $ECP\n')
            for atom in self.atoms:
                inp.write(atom+'-ecp\n')
            inp.write(' $END\n')
            inp.close()
        
    def mol_get_emsl_mixed_basis(self,elements):

        """ 
        IN:  gathers requested basis-sets from the local EMSL database
        OUT: returns '2D list' containing the symbol, name of basis-set, and basis-set
             for each element
        """
        
        self.basis_set = elements
        for num,element in enumerate(elements):
            self.basis_set[num].append(
                pyg_get_emsl_basis_for_element(element[0],element[1])[0])
