# import the pyegms molecule class, pyg_molecule
from pyegms import pyg_molecule

# path of inp, log or xyz file containing molecular coordinates
path = 'c2v_water_RHF_6-31Gd_geom_opt.log'

# path of new input to be generated
new_input = 'c2v_water_MP2_aug-ccPVTZ_geom_opt.log'

# new instance of molecule class; pass the inp/log/xyz file path
stilbene = pyg_molecule(path)

# choose a basis set for each element-type.
mixed_basis = [['H','6-31G*'],['C','6-31G*'],['Pt','LANL08'],['N','6-31G*']]

# pyegms takes advantage of ebsel (EMSL_Basis_Set_Exchange_Local), an API for, and
# local version of, th EMSL basis-set exchange database.

# the following pyg_molecule class member function loads the basis-sets defined in
# 'mixed_basis' into the self.basis-set variable (i.e. stilbene.basis-set)
stilbene.mol_get_emsl_mixed_basis(mixed_basis)

# punch the coordinates from the original inp/log/xyz file
stilbene.mol_write_data_group(new_input,'your comment goes here',False)

# punch the new basis-set EXPLICITLY using the '$basis basnam(1)=...' input method in GAMESS
stilbene.mol_write_basis_group(new_input)

# punch an ECP group if needed (as well as '$contrl pp=read $end')
stilbene.mol_write_ecp_group(new_input)
